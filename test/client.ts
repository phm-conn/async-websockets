/// <reference no-default-lib="true" />
/// <reference lib="deno.worker" />
import { AsyncWebSocket } from "./../mod.ts";
import * as test from "https://deno.land/std@0.133.0/testing/asserts.ts";
{
  const socket = new AsyncWebSocket(new WebSocket("ws://127.0.0.1:8000"));
  await socket.holdTilOpen();
  let num = 0;
  try {
    for await (const message of socket.holdForMessages()) {
      const data = message.data;
      console.log(`Clients echoing ${data}`);
      socket.send(num + "");
      num++;
    }
  } catch (_e) {
    null;
  }
  const close = await socket.holdTilClosed();
  console.log(close);
  console.log(`Client1 Closing.`);
}
{
  const socket = new AsyncWebSocket(new WebSocket("ws://127.0.0.1:8000"));
  await socket.holdTilOpen();
  let num = 0;
  try {
    for await (const message of socket.holdForMessages()) {
      const data = message.data;
      console.log(`Clients echoing ${data}`);
      socket.send(num + "");
      num++;
      if (num > 4) {
        socket.close(3050);
      }
    }
  } catch (_e) {
    null;
  }
  const close = await socket.holdTilClosed();
  console.log(close);
  console.log(`Client2 Closing.`);
}
