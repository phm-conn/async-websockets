/// <reference no-default-lib="true" />
/// <reference lib="deno.worker" />
import { Application } from "https://deno.land/x/oak@v10.5.1/mod.ts";
import { AsyncWebSocket } from "./../mod.ts";
import * as test from "https://deno.land/std@0.133.0/testing/asserts.ts";

const killer = new AbortController();
const app = new Application();
app.use(async (ctx) => {
  ctx.response.headers.append("Access-Control-Allow-Origin", "*");
  const socket = new AsyncWebSocket(await ctx.upgrade());
  await socket.holdTilOpen();
  let num = 0;
  console.log(`servers sending ${num}`);
  socket.send(num + "");
  try {
    for await (const message of socket.holdForMessages()) {
      const data = message.data;
      console.log(`server got back ${data}`);
      num++;
      console.log(`servers sending ${num}`);
      socket.send(num + "");
      if (num > 5) {
        socket.close(3025);
      }
    }
  } catch (e) {
    console.log(e);
  }
  const close = await socket.holdTilClosed();
  console.log(close);
  //killer.abort("Done.");
  console.log("Server Can't kill iteself.");
});

const serv = app.listen({
  port: 8000,
  signal: killer.signal,
});
console.log("Server Ready!");
await serv;
console.log("Done!");
