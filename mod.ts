export type WebSocketData = string | ArrayBufferLike | Blob | ArrayBufferView;
import {
  Deferred,
  deferred,
} from "https://deno.land/std@0.135.0/async/deferred.ts";
import { abortableAsyncIterable } from "https://deno.land/std@0.135.0/async/abortable.ts";

export class AsyncWebSocket {
  private sock: WebSocket;

  private closeEvent?: { code?: number; reason?: string };

  private messageListeners: Deferred<MessageEvent<WebSocketData>>[] = [];
  private messageQueue: MessageEvent<WebSocketData>[] = [];
  private closeQueue: Deferred<void>[] = [];
  private openQueue: Deferred<void>[] = [];
  private errorQueue: Deferred<Event | ErrorEvent>[] = [];
  constructor(socket: WebSocket) {
    this.sock = socket;
    this.sock.onopen = (e: Event) => {
      this.handleOpen(e);
    };
    this.sock.onmessage = (e: MessageEvent<WebSocketData>) => {
      this.handleMessage(e);
    };
    this.sock.onerror = (e: Event | ErrorEvent) => {
      this.handleError(e);
    };
    this.sock.onclose = (e: CloseEvent) => {
      this.handleClose(e);
    };
  }

  private handleOpen(_e: Event) {
    for (const def of this.openQueue) {
      def.resolve();
    }
  }
  private handleError(e: Event | ErrorEvent) {
    for (const def of this.errorQueue) {
      def.resolve(e);
    }
  }
  private handleMessage(e: MessageEvent<WebSocketData>) {
    const next = this.messageListeners.shift();
    if (next) {
      next.resolve(e);
    } else {
      this.messageQueue.push(e);
    }
  }
  private handleClose(e: CloseEvent) {
    const c = {
      code: e.code,
      reason: e.reason,
    };
    this.closeEvent = c;
    for (const def of this.closeQueue) {
      def.resolve();
    }
  }

  async holdTilError() {
    const error = deferred<Event | ErrorEvent>();
    this.errorQueue.push(error);
    return await error;
  }

  get isOpen() {
    return this.sock.readyState == WebSocket.OPEN;
  }

  get socket() {
    return this.sock;
  }

  async holdTilOpen() {
    const open = deferred<void>();
    this.openQueue.push(open);
    if (this.sock.readyState == WebSocket.OPEN) {
      return;
    }
    await open;
    return;
  }

  async holdTilClosed() {
    const close = deferred<void>();
    this.closeQueue.push(close);
    if (this.sock.readyState == WebSocket.CLOSED) {
      if (!this.closeEvent) {
        throw new Error("Invalid Close State!");
      }
      return this.closeEvent;
    }
    await close;
    if (!this.closeEvent || this.sock.readyState != WebSocket.CLOSED) {
      throw new Error("Invalid Close State!");
    }
    return this.closeEvent;
  }

  private async *internalHoldForMessages(): AsyncGenerator<
    MessageEvent<WebSocketData>
  > {
    let next = deferred<MessageEvent<WebSocketData>>();
    this.messageListeners.push(next);
    while (true) {
      let message: MessageEvent<WebSocketData> | undefined = await next;
      yield message;
      next = deferred<MessageEvent<WebSocketData>>();
      this.messageListeners.push(next);
      message = this.messageQueue.shift();
      while (message != undefined) {
        yield message;
        message = this.messageQueue.shift();
      }
    }
  }

  public holdForMessages() {
    const c = new AbortController();
    const close = deferred<void>();
    this.closeQueue.push(close);
    close.then(() => {
      c.abort();
    });
    return abortableAsyncIterable(this.internalHoldForMessages(), c.signal);
  }

  public send(data: WebSocketData) {
    if (this.isOpen) {
      return this.sock.send(data);
    }
  }

  public close(code = 1000, reason?: string) {
    if (
      !(
        code == 1000 ||
        (
          code >= 3000 &&
          code <= 4999
        )
      )
    ) {
      throw new Error("Invalid Exit Code!");
    }
    const c = {
      code,
      reason,
    };
    this.closeEvent = c;
    for (const def of this.closeQueue) {
      def.resolve();
    }
    return this.sock.close(code, reason);
  }
}
